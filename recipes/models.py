from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)  # can be blank, not required
    created = models.DateTimeField(auto_now_add=True)
    # content = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    origin = models.TextField(null=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.abbreviation + " is short for: " + self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(
        validators=[
            MaxValueValidator(20),
            MinValueValidator(0),
        ]
    )
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure", related_name="ingredients", on_delete=models.PROTECT
    )
    food = models.ForeignKey(
        "FoodItem", related_name="ingredients", on_delete=models.PROTECT
    )

    def __str__(self):
        return (
            self.recipe.name
            + " :"
            + str(self.amount)
            + " "
            + self.measure.abbreviation
            + " of "
            + self.food.name
        )


class Step(models.Model):
    order = models.SmallIntegerField()
    directions = models.TextField(max_length=300)
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    food_items = models.ManyToManyField("FoodItem", blank=True)

    def __str__(self):
        return (
            self.recipe.name
            + "'s step "
            + str(self.order)
            + ": "
            + self.directions
        )


class Rating(models.Model):
    value = models.SmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    recipe = models.ForeignKey(
        "Recipe", related_name="ratings", on_delete=models.CASCADE
    )
