from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    paginate_by = 2

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring == None:
            querystring = ""
        return Tag.objects.filter(name__icontains=querystring)


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"


class TagCreateView(CreateView):
    model = Tag
    template_name = "tags/new.html"
    fields = "__all__"
    success_url = reverse_lazy("tags_list")

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context[""]


class TagUpdateView(UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    fields = "__all__"
    # success_url = reverse_lazy("tag_detail", kwargs={"pk": pk})
    success_url = reverse_lazy("tags_list")


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
